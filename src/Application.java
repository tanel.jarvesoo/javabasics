import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        compareUserInput();

    }

    public static void printHello() {
        System.out.print("Hello, world\nHello, world\n");
    }

    public static void roundUpValue() {
        double a = 13.3333;
        double roundUp = (double) Math.round(a * 100) / 100;
        System.out.println(roundUp);
    }

    public static void roundUpDecimal() {
        double a = 13.3333;
        System.out.printf("%.2f", a);
        System.out.println();
    }

    public static void alignTextToRight() {
        String text = "Align me to the left";
        String text2 = "Align me to the right";
        System.out.printf("%-35s %20s", text, text2);
        System.out.println();
    }

    public static void divideTwoNumbers() {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Please enter the first number");
        int a = userInput.nextInt();
        System.out.println("Please enter the second number");
        int b = userInput.nextInt();
        double c = (double) a / b;
        System.out.printf("The answer is %.3f", c);
        System.out.println();
    }

    public static void maxValues() {
        int max = Integer.MAX_VALUE;
        long result = (long) max + max;
        System.out.println(result);
    }

    public static void userInput() {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Please enter a float character");
        float a = userInput.nextFloat();
        System.out.println("Please enter a byte character");
        byte b = userInput.nextByte();
        System.out.println("Please enter a char character");
        char c = userInput.next().charAt(0);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }

    public static void compareUserInput() {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Please enter a number");
        float a = userInput.nextFloat();
        if (a < 30) {
            System.out.println("The number you entered is lower than 30");
        } else if (a > 30) {
            System.out.println("The number you entered is greater than 30");
        } else if (a == 30) {
            System.out.println("The number you entered is 30");
        }
    }
    public static void compareMultipleInputs() {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Please enter the first number");
        float a = userInput.nextFloat();
        System.out.println("Please enter the second number");
        float b = userInput.nextFloat();
        boolean x = a > 30 && b > 30;
        boolean y = a > 30 && b < 30;
        boolean z = a > 30 && b == 30;
        boolean x1 = a < 30 && b < 30;
        boolean y2 = a < 30 && b > 30;
        boolean z1 = a < 30 && b == 30;

    }

}
